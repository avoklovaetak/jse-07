package ru.volkova.tm;

import ru.volkova.tm.constant.ArgumentConst;
import ru.volkova.tm.constant.TerminalConst;
import ru.volkova.tm.model.Command;
import ru.volkova.tm.util.NumberUtil;

import javax.annotation.processing.SupportedSourceVersion;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg){
        if (arg == null) return;
        switch (arg){
            case ArgumentConst.ARG_ABOUT: showAbout(); break;
            case ArgumentConst.ARG_HELP: showHelp(); break;
            case ArgumentConst.ARG_VERSION: showVersion(); break;
            case ArgumentConst.ARG_INFO: showSystemInfo(); break;
            default: showIncorrectArgument();
        }
    }

    private static void parseCommand(final String command){
        if (command == null) return;
        switch (command){
            case TerminalConst.CMD_ABOUT: showAbout(); break;
            case TerminalConst.CMD_HELP: showHelp(); break;
            case TerminalConst.CMD_VERSION: showVersion(); break;
            case TerminalConst.CMD_INFO: showSystemInfo(); break;
            case TerminalConst.CMD_EXIT: exit();
            default: showIncorrectCommand();
        }
    }

    private static void exit(){
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static boolean parseArgs(String[] args){
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showIncorrectCommand(){
        System.out.println("Error! Command not found...");
    }

    private static void showIncorrectArgument(){
        System.out.println("Error! Argument not found...");
    }

    private static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ekaterina Volkova");
        System.out.println("E-MAIL: avoklovaetak@yandex.ru");
    }

    private static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showHelp(){
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void showSystemInfo(){
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.format(memoryTotal));
        final long usedMemory = memoryTotal - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
        System.out.println("[OK]");
    }
    
}
